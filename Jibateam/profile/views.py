from django.shortcuts import render, get_object_or_404
from Jibateam.profile.models import Profil
from django.contrib.auth import logout, authenticate, login
from django.contrib.auth.decorators import login_required
from django.http import HttpResponseRedirect
from Jibateam.forms import ConnexionForm
from django.template import RequestContext


def profile(request, uname):
    profil = get_object_or_404(Profil, username=uname)
    return render(request, 'profile.html', locals(), context_instance=RequestContext(request))

def login_view(request):
    connexion_form = ConnexionForm()
    if request.method == 'POST':
        uname=request.POST.get('username')
        pwd=request.POST.get('password')
        user = authenticate(username=uname, password=pwd)
        if user :
            login(request, user)
            return HttpResponseRedirect('/profile/%s' % uname)
        return render(request, 'login.html', {
            'login_form' : connexion_form,
            'retry' : True,
            }, context_instance=RequestContext(request))
    return render(request, 'login.html', {
                                         'login_form':connexion_form,
                                         'retry' : False,
                                        }, context_instance=RequestContext(request))

@login_required
def logout_view(request):
    logout(request)
    return HttpResponseRedirect('/')