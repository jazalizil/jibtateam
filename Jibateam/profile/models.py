from django.db import models
from django.contrib.auth.models import AbstractUser, BaseUserManager
from multiselectfield import MultiSelectField

class ProfilManager(BaseUserManager):
    def create_superuser(self, username, email, password):
        """
        Creates and saves a superuser with the given email, date of
        birth and password.
        """
        user = self.create_user(username=username,
                                email=ProfilManager.normalize_email(email),
                                password=password,
                                )
        user.is_staff = True
        user.is_superuser = True
        user.save(using=self._db)
        return user

    def create_user(self, username, email, password=None):
        if not email:
            raise ValueError("We need an e-mail here...")

        user = self.model(
                          username=username,
                          email = ProfilManager.normalize_email(email),
                          poids=30,
                          taille=130,
                          age=21,
                          sport='basket',
                          level=5,
                          team_id=42,
                          points=0,
                          corpulence='muscle',
                          travel='car',
                          )
        user.set_password(password)
        user.save(using=self._db)
        return user

class Choices(models.Model):
    choice = models.CharField(max_length=42)

class Profil(AbstractUser):
    SPORT_CHOICES = (('basket', 'Basketball'),
                ('foot', 'Football'))
    TRAVEL_CHOICES = (('scoot', 'Scooter'),
                ('car', 'Voiture'),
                ('bike', 'Velo'),
                ('tec', 'Transport en commun'))
    CORPULENCE_CHOICES = (('fin', 'Cure-dent'),
                    ('svelte', 'Svelte'),
                    ('muscle', 'Musclor'),
                    ('fat', 'Bouboule'))
    POSTEBB_CHOICES = (('all', 'Peu importe'),
                       ('meneur', 'Meneur'),
                       ('ailier', 'Ailier'),
                       ('pivot', 'Pivot'))
    POSTEFB_CHOICES = (('all', 'Peu importe'),
                       ('goal', 'Goal'),
                       ('def', 'Defenseur'),
                       ('mil', 'Milieu'),
                       ('att', 'Attaquant'))
    corpulence = MultiSelectField(max_length=42, default='muscle', choices=CORPULENCE_CHOICES)
    travel = MultiSelectField(max_length=42, default='car', choices=TRAVEL_CHOICES)
    team = models.CharField(max_length=42, default='TeamTokyo')
    sport = MultiSelectField(max_length=42, default='basket', choices=SPORT_CHOICES)
    poids = models.PositiveIntegerField(default=80)
    taille = models.PositiveIntegerField(default=180)
    img = models.ImageField(upload_to='img/usr', null=True, blank=True)
    age = models.IntegerField(blank=True, default=21)
    description = models.TextField(max_length=5000, default='Team admin')
    level = models.PositiveSmallIntegerField(blank=True)
    team_id = models.IntegerField(blank=True)
    poste_fb = MultiSelectField(max_length=42, choices=POSTEFB_CHOICES, default='all')
    poste_bb = MultiSelectField(max_length=42, choices=POSTEBB_CHOICES, default='all')
    points = models.IntegerField(blank=True)
    terrain = models.CharField(blank=True, max_length=42)
    has_a_team = models.BooleanField(default=False)