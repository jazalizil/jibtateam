from django.conf.urls import patterns, include, url
from django.contrib import admin
from django.conf.urls.static import static
from django.conf import settings
from Jibateam.forms import RegisterWizard, RegisterForm1, RegisterForm2, RegisterForm3

admin.autodiscover()

urlpatterns = patterns('Jibateam.views',
    url(r'^$', 'index', name='index'),
    url(r'^contact/$', 'contact', name='contact'),
    #url(r'^register/$', 'register'),
    # Examples:
    # url(r'^$', 'Jibateam.views.home', name='home'),
    # url(r'^Jibateam/', include('Jibateam.Jibateam.urls')),

    # Uncomment the admin/doc line below to enable admin documentation:
    # url(r'^admin/doc/', include('django.contrib.admindocs.urls')),

    # Uncomment the next line to enable the admin:
    url(r'^admin/', include(admin.site.urls)),
)

urlpatterns += patterns('Jibateam.forms',
                        url(r'^register/$', RegisterWizard.as_view([RegisterForm1, RegisterForm2, RegisterForm3]), name='register'),
                        )

urlpatterns += patterns('Jibateam.profile.views',
                        url(r'^profile/(\w+)$', 'profile', name='profile'),
                        url(r'^login/$', 'login_view', name='login_view'),
                        url(r'^logout/$', 'logout_view', name='logout_view'),
                        )

urlpatterns += static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)
