from django.shortcuts import render
from Jibateam.forms import ConnexionForm
from django.contrib.auth import login, authenticate
from django.template import RequestContext

def login_form(request):
    return {'login_form':ConnexionForm()}