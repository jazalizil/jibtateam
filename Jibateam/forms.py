# -*- coding: utf-8 -*-

from django.db import models
import floppyforms as forms
from django.forms.widgets import Input
import string
from PIL.Image import core as _imaging
from Jibateam.profile.models import Profil
from django.contrib.formtools.wizard.views import SessionWizardView
from django.shortcuts import render_to_response, render
from django.core.files.storage import FileSystemStorage
import os.path
from Jibateam import settings

BOOTSTRAP = {'class' : 'form-control'}
BOOTSTRAP_OPT = {'class' : 'form-control', 'placeholder' : 'Optionnel'}
BOOTSTRAP_AGE = {'class' : 'form-control age'}
BOOTSTRAP_POIDS = {'class' : 'form-control poids'}
BOOTSTRAP_TAILLE = {'class' : 'form-control taille'}
class Slider(forms.RangeInput):
    """ Redef de <input type=range /> """
    step = 1

    def __init__(self, x, y, *args, **kwargs):
        self.min = x
        self.max = y
        super(Slider, self).__init__(*args, **kwargs)

    
class RegisterWizard(SessionWizardView):
    file_storage = FileSystemStorage(location=os.path.join(settings.MEDIA_ROOT, 'img'))
    def done(self, form_list, **kwargs):
        data = {};
        for form in form_list:
            data.update(form.cleaned_data)
        profil = Profil(
            username=data['uname'],
            email=data['email'],
            password=data['password'],
            corpulence=data['corpulence'],
            travel=data['travel'],
            team=data['team'],
            sport=data['sport'],
            poids=data['poids'],
            taille=data['taille'],
            age=data['age'],
            description=data['description'],
            img=data['user_img'],
            level=0,
            team_id=0,
            points=0,
            terrain='null',
            has_a_team = True if data['team'] else False,
            poste_fb=data['poste_fb'],
            poste_bb=data['poste_bb'],
        )
        profil.save()
        return render_to_response('profile.html', {'username' : profil.username, 'password' : profil.password})
        
    def get_template_names(self):
        return 'register.html'


class RegisterForm1(forms.Form):
    uname = forms.CharField(label=u'Nom d\'utilisateur', widget=forms.TextInput(attrs=BOOTSTRAP), max_length=20, required=True)
    password = forms.CharField(label=u'Mot de passe', widget=forms.PasswordInput(attrs=BOOTSTRAP, render_value=False), required=True)
    passconf = forms.CharField(label=u'Confirmation', widget=forms.PasswordInput(attrs=BOOTSTRAP, render_value=False), required=True)
    email = forms.EmailField(label=u'Adresse email', required=True, widget=forms.EmailInput(attrs=BOOTSTRAP))

    def clean_password(self):
        cleaned_data=super(RegisterForm1,self).clean()
        passwd = cleaned_data.get('password')
        print 'clean_password -> passwd = ', passwd
        numbers = list(string.digits)
        num_check = False
        for c in passwd:
            if c in numbers:
                num_check = True
                break
        if not num_check or len(passwd) < 8:
            self._errors["password"] = self.error_class(["Ce champ doit contenir au moins huit caracteres, dont \
                        un chiffre"])
        return passwd

    def clean_passconf(self):
        cleaned_data=super(RegisterForm1,self).clean()
        pwd = cleaned_data.get('password')
        pwd_conf = cleaned_data.get('passconf')
        if pwd and pwd_conf and pwd != pwd_conf:
            self._errors["passconf"] = self.error_class(["La confirmation ne correspond pas au mot de passe"])
        del cleaned_data['passconf']
        return pwd_conf

    def clean_email(self):
        cleaned_data = super(RegisterForm1, self).clean()
        adr = cleaned_data.get('email')
        uname = cleaned_data.get('uname')
        usr = Profil.objects.filter(email=adr).exclude(username=uname)
        if usr.count() > 0:
            raise forms.ValidationError(u'L\'adresse email indiquee est deja utilisee')
        return adr

    def clean_uname(self):
        cleaned_data = super(RegisterForm1, self).clean()
        uname = cleaned_data.get('uname')          
        usr = Profil.objects.filter(username=uname)
        if usr.count() > 0:
            raise forms.ValidationError(u'Le nom d\'utilisateur indique est deja utilise')
        return uname

class RegisterForm2(forms.Form):
    sport = forms.MultipleChoiceField(label=u'Sport(s)\n pratique(s)', required=True, widget=forms.CheckboxSelectMultiple(attrs=BOOTSTRAP), choices=Profil.SPORT_CHOICES)
    travel = forms.MultipleChoiceField(label=u'Moyen de locomotion', required=True, widget=forms.CheckboxSelectMultiple(attrs=BOOTSTRAP), choices=Profil.TRAVEL_CHOICES)
    team = forms.CharField(label=u'Equipe', required=False, max_length=42, widget=forms.TextInput(attrs=BOOTSTRAP))
    age = forms.IntegerField(widget=Slider('7', '77', attrs=BOOTSTRAP_AGE), label=u'Age', initial='7')
    poids = forms.IntegerField(label=u'Poids', initial='30', widget=Slider('30', '200', attrs=BOOTSTRAP_POIDS))
    taille = forms.IntegerField(label=u'Taille', initial='130', widget=Slider('130', '230', attrs=BOOTSTRAP_TAILLE))
    poste_fb = forms.MultipleChoiceField(label=u'Poste Football', widget=forms.CheckboxSelectMultiple, choices=Profil.POSTEFB_CHOICES)
    poste_bb = forms.MultipleChoiceField(label=u'Poste Basket - Ball', widget=forms.CheckboxSelectMultiple, choices=Profil.POSTEBB_CHOICES)
    corpulence = forms.ChoiceField(required=True, widget=forms.RadioSelect(attrs=BOOTSTRAP), choices=Profil.CORPULENCE_CHOICES)
    tel = forms.CharField(required=False, widget=forms.PhoneNumberInput(attrs=BOOTSTRAP_OPT), max_length=10, min_length=10)

class RegisterForm3(forms.Form) :
    user_img = forms.ImageField(label=u'Image de profil', help_text=u'Choisissez un fichier image', required=False)
    description = forms.CharField(required=True, widget=forms.Textarea(attrs={'rows':1, 'class':'form-control'}), label=u'Description')


class ConnexionForm(forms.Form):
    username = forms.CharField(label=u'Nom d\'utilisateur ou email', max_length=30, widget=forms.TextInput(attrs={'placeholder' : 'Login ou email', 'class':'form-control'}))
    password = forms.CharField(label=u'Mot de passe', widget=forms.PasswordInput(attrs={'placeholder' : 'Mot de passe', 'class':'form-control'}), min_length=8)
    remember = forms.BooleanField(label=u'Se souvenir de moi', required=False)

class ContactForm(forms.Form):
    username = forms.CharField(label=u'Nom d\'utilisateur', max_length=20, required=True, widget=forms.TextInput(attrs=BOOTSTRAP))
    email = forms.EmailField(label=u'Adresse email', required=True, widget=forms.EmailInput(attrs=BOOTSTRAP))
    message = forms.CharField(required=True, widget=forms.Textarea(attrs={'rows':1, 'class':'form-control'}), max_length=1000)

def __unicode__(self):
    """"""
    return u"%s" % self.titre
