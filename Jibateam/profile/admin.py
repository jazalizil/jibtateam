from django.contrib import admin
from Jibateam.profile.models import Profil

class ProfilAdmin(admin.ModelAdmin):
    list_display=['username', 'date_joined', 'has_a_team']
    search_fields=['date_joined']

admin.site.register(Profil, ProfilAdmin)