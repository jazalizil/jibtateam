from django.shortcuts import render
from Jibateam.forms import ContactForm
from django.template import RequestContext


def index(request):
    return render(request, 'index.html', context_instance=RequestContext(request))

def contact(request):
    send = False
    if request.method == 'POST':
        contact_form = ContactForm(request.POST)
        if contact_form.is_valid():
            uname = contact_form.cleaned_data['username']
            email = contact_form.cleaned_data['email']
            message = contact_form.cleaned_data['message']
            print(uname, email, message)
            send = True
    else:
        contact_form = ContactForm()
    return render(request, 'contact.html', locals(), context_instance=RequestContext(request))